<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('casts.add');
    }

    public function store(Request $request)
    {
        // Validasi 
        $request->validate([
            'name' => 'required|max:25',
            'umur' => 'required|max:3',
            'bio' => 'required|max:512',
        ]);

        // Insert to DB
        DB::table('cast')->insert([
            'name' => $request->input('name'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        // Return Redirect
        return redirect('/cast');
    }

    public function show()
    {
        // Read Data from DB 
        $casts = DB::table('cast')->get();
 
        return view('casts.show', ['cast' => $casts]);
    }

    public function detail($id)
    {
        $casts = DB::table('cast')->find($id);

        return view('casts.detail', ['cast' => $casts]);
    }

    public function edit($id)
    {
        $casts = DB::table('cast')->find($id);

        return view('casts.edit', ['cast' => $casts]);
    }

    public function update($id, Request $request)
    {
        // Validasi 
        $request->validate([
            'name' => 'required|max:25',
            'umur' => 'required|max:3',
            'bio' => 'required|max:512',
        ]);

        // Update to DB
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request->input('name'),
                    'umur' => $request->input('umur'),
                    'bio' => $request->input('bio')
                ]
            );
        

        // Return Redirect
        return redirect('/cast');
    }


    public function delete($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
