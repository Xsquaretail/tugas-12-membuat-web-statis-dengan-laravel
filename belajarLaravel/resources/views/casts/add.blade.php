@extends('layouts.master')
@section('title')
    Halaman Tambah Cast
@endsection

@section('subtitle')
    Tambah Cast
@endsection

@section('content')
    <form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" name="name">
    </div>

    @error('name')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Umur Cast</label>
        <input type="text" class="form-control col-sm-1 @error('title') is-invalid @enderror" name="umur">
    </div>

    @error('umur')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" class="form-control @error('title') is-invalid @enderror" id="" cols="30" rows="10"></textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection