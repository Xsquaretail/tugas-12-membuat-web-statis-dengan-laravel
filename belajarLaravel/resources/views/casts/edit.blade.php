@extends('layouts.master')
@section('title')
    Halaman Edit Cast
@endsection

@section('subtitle')
    Edit Cast
@endsection

@section('content')
    <form action="/cast/{{ ($cast->id) }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{ ($cast->name) }}" name="name">
    </div>

    @error('name')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label>Umur Cast</label>
        <input type="text" class="form-control col-sm-1 @error('title') is-invalid @enderror" value="{{ ($cast->umur) }}" name="umur">
    </div>

    @error('umur')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <div class="form-group">
        <label >Bio</label>
        <textarea name="bio" class="form-control @error('title') is-invalid @enderror" id="" cols="30" rows="10">{{ ($cast->bio) }}</textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger" role="alert">
        {{ ($message) }}
    </div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection