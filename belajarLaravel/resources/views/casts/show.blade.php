@extends('layouts.master')
@section('title')
    Halaman Tampil Cast
@endsection

@section('subtitle')
    Tampil Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary my-3">Add</a>

    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Umur</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $value)
                <tr>
                    <th scope="row">{{ ($key + 1) }}</th>
                    <td>{{ ($value->name) }}</td>
                    <td>{{ ($value->umur) }}</td>
                    <td>
                        <form action="/cast/{{ ($value->id) }}" method="POST">
                        <a href="/cast/{{ ($value->id) }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{ ($value->id) }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Cast Kosong Silahkan Tambah Data</td>
                </tr>
            @endforelse

          
        </tbody>
      </table>
@endsection