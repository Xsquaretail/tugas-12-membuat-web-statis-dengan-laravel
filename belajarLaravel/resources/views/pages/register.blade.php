@extends('layouts.master')
@section('title')
    Register Page
@endsection

@section('subtitle')
    Register
@endsection

@section('content')
    <h1>Create New Account!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
            <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
            <input type="text" name='lname'><br><br>

        <label>Gender</label><br><br>
            <input type="radio" name="gender" id="gender" value="1">
        <label>Male</label><br>
            <input type="radio" name="gender" id="gender" value="2">
        <label>Female</label><br>
            <input type="radio" name="gender" id="gender" value="3">
        <label>Other</label><br><br>

        <label>Nationality:</label><br><br>
            <select name="nationality">
                <option value="1">Indonesian</option>
                <option value="2">Chinese</option>
                <option value="3">Indian</option>
            </select><br><br>

        <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="language" value="1">
        <label>Bahasa Indonesia</label><br>
            <input type="checkbox" name="language" value="2">
        <label>English</label><br>
            <input type="checkbox" name="language" value="3">
        <label>Other</label><br><br>

        <label>Bio:</label><br><br>
            <textarea cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection
    