@extends('layouts.master')
@section('title')
    Welcome Page
@endsection

@section('subtitle')
    Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{$firstName}} {{$lastName}}!</h1>
    <h2>Terima Kasih telah bergabung di sanberbook. Social Media Kita bersama!</h2>
@endsection