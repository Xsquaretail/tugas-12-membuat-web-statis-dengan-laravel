<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/master', function(){
    return view('layouts.master');
});

Route::get('/data-table', function(){
    return view('pages.data-table');
});

Route::get('/table', function(){
    return view('pages.table');
});


// CRUD Cast

// c => Create Data
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);

// r => Read Data 
Route::get('/cast', [CastController::class, 'show']);
Route::get('/cast/{id}', [CastController::class, 'detail']);

// u => Update Data 
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

// d => Delete Data 
Route::delete('/cast/{id}', [CastController::class, 'delete']);